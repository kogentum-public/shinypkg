# shinypkg

This package relates to my talk given at the BURN Meetup on Febr. 06, 2019. 

## Installation

The package is not intended for real use (see Purpose). I suggest to clone this repository instead and investigate its content, or use it as a skeleton in your own package. However, you can of course install it and even launch the application to have some fun. 

The easiest way is to use the *remotes* package (if you have a not-too-old *devtools* installed, you also have *remotes*):
```r
if (requireNamespace("remotes")) {
    remotes::install_gitlab("kogentum-public/shinypkg")
}
```

After installing the package, you can run the application by:
```r
shinypkg::launch()
```

The application is of course pretty basic; it displays a funny quote (from the *fortunes* package) and a table of the generated quotes. 

![screenshot](https://gitlab.com/kogentum-public/shinypkg/uploads/0abc5ec9df09da4b560f5687670608e2/screenshot.png)

## Purpose

*shinypkg* demonstrates how one can create a package from a Shiny application instead of using ui.R/server.R/... structure. It also demonstrates some solutions which I consider best practices when developing Shiny applications. 

Problems with the standard structure:    
*  it must be ensured that the required packages are available on the host
*  global namespace is cluttered after running the application
*  namespace clashes can occur
*  documentation is usually poor
*  code is often hard to read 

If the application is developed as a package:
*  it can be installed just like other R packages, dependencies are installed automatically
*  global namespace is not cluttered when the app is launched
*  precise definition of what functions are imported from which packages is easy
*  documentation, unit tests are easy to add

Some other tips included in *shinypkg* are not bound to being a package, but 
rather how the code is organized. The key messages are:
*  use modules whenever you can
*  write separate functions for the particular functionalities so that your server functions describe only the logic of the module/app and do not contain the actual implementation of the particular functionality

The key principle is that one should write modular code which is easy to read and maintain. 
